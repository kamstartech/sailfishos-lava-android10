

TARBALL=ubuntu-focal-20210531-android-rootfs.tar.bz2

curl -O https://releases.sailfishos.org/ubu/$TARBALL

UBUNTU_CHROOT=$PLATFORM_SDK_ROOT/sdks/ubuntu

sudo mkdir -p $UBUNTU_CHROOT

sudo tar --numeric-owner -xjf $TARBALL -C $UBUNTU_CHROOT

ubu-chroot -r $PLATFORM_SDK_ROOT/sdks/ubuntu

sudo mkdir -p $ANDROID_ROOT

sudo chown -R $USER $ANDROID_ROOT

cd $ANDROID_ROOT

repo init -u https://github.com/mer-hybris/android.git -b hybris-17.1

